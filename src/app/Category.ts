import { Product } from './Product';

export class Category {
  name : string;
  products : Product[];
  price : string[];
  pricename : string[];

}