import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Location } from '@angular/common';

import { Product } from '../Product';
import { Category } from '../Category';
import { sampleproducts } from '../sampleproducts';


@Component({
  selector: 'app-category-display',
  templateUrl: './category-display.component.html',
  styleUrls: ['./category-display.component.css']
})

export class CategoryDisplayComponent implements OnInit {


  @Input() count : number; 
  @Input() cat : Category; 
  @Input() in : number;



  constructor(
  		private route: ActivatedRoute,
  		private location: Location,
      private router: Router

  ) {}


   	getc(): void {
 				const id = +this.route.snapshot.paramMap.get('id');
  				this.in = id;
 }
 getCate(): void {
 	this.cat = sampleproducts[this.in];
 }

  getCat(): void {
  if(!this.cat)
  {
  	if(!this.in)
  		this.getc();
  	this.getCate();
  }
  if(!this.count)
  		this.count = 7;
  }
  
  ngOnInit(){
  	this.getCat();

  }

}
