import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RightSideComponent } from './right-side/right-side.component';
import { CategoryDisplayComponent } from './category-display/category-display.component';
import { ProductDisplayComponent } from './product-display/product-display.component';
import { SearchDisplayComponent } from './search-display/search-display.component';


const routes: Routes = [
  { path: '', component: RightSideComponent },
    { path: 'category/:id1/product/:id2', component: ProductDisplayComponent },
  { path: 'category/:id', component:CategoryDisplayComponent },
  { path: 'category/:id1/search/:id2', component:SearchDisplayComponent }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
