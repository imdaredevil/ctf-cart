import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Product } from '../Product';
import { Category } from '../Category';
import { sampleproducts } from '../sampleproducts';

@Component({
  selector: 'app-product-display',
  templateUrl: './product-display.component.html',
  styleUrls: ['./product-display.component.css']
})
export class ProductDisplayComponent implements OnInit {

	prin : number;
	cain : number;
	Product1 : Product;

  constructor(
  		private route: ActivatedRoute,
  		private location: Location

  ) {}

  setVariables() : void {
  		const id1 = +this.route.snapshot.paramMap.get('id1');
  		const id2 = +this.route.snapshot.paramMap.get('id2');
  				this.Product1 = sampleproducts[id1].products[id2];
  				this.cain = id1;
  				this.prin = id2;
  }

  ngOnInit() {
  	this.setVariables();
  }

}
