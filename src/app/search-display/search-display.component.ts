import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { Location } from '@angular/common';

import { Product } from '../Product';
import { Category } from '../Category';
import { sampleproducts } from '../sampleproducts';



@Component({
  selector: 'app-search-display',
  templateUrl: './search-display.component.html',
  styleUrls: ['./search-display.component.css']
})
export class SearchDisplayComponent implements OnInit {

 
 	cat : Category;
  result: Product[];
  in : number;

  constructor(
  		private route: ActivatedRoute,
  		private location: Location,

  ) { }


  getCat() : void {
  	var catin = this.route.snapshot.paramMap.get('id1');
    this.cat = sampleproducts[catin];
    var pros = this.cat.products;
    this.in = +catin;
    var critst = this.route.snapshot.paramMap.get('id2');
    var crite = JSON.parse(critst);
    var pricecrit = crite.price;
    var specs = crite.specs;
    var crits = pricecrit.split(':'); 
    var res = new Array();
    for(var i=0;i<pros.length;i++)
    {
        if(pros[i].price <= +crits[1] && pros[i].price >= +crits[0])
        {
            var k = 5;
            res.push(pros[i]);
        }

    }
    console.log(res);
    this.result = res;
  }

  ngOnInit() {
  		this.getCat();
  }

}
