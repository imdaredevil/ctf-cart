import { Product } from './Product';
import { Category } from './Category';

export const sampleproducts : Category[] = [
	{ name : 'Laptops' , products : [
	  { id : 1, name : "inspiron-i5", image: "1.jpeg", price : 40000, Company : "dell"},
	  { id : 2, name : "nvidia-i7", image: "2.jpeg", price : 30000, Company : "asus"},
	  { id : 3, name : "flex-yoga", image: "3.jpeg", price : 60000, Company : "lenovo"},
	  { id : 4, name : "machine", image: "4.jpeg", price : 35000, Company : "toshiba"},
	  { id : 5, name : "smart series", image: "5.jpeg", price : 80000, Company : "acer"},
	  { id : 6, name : "alienware", image: "6.jpeg", price : 150000, Company : "dell"},
	  { id : 7, name : "R.O.G. series", image: "7.jpeg", price : 200000, Company : "asus"},
	],
		price : ['0:50000','50000:100000','100000:10000000'],
		pricename : ['less than 50000','50000-100000','Above 100000'] },
	{ name : 'Mobiles' , products : [
	  { id : 1, name : "1100-basic", image: "11.jpeg", price : 2000, Company : "Nokia"},
	  { id : 2, name : "note 5 pro", image: "12.jpeg", price : 13000, Company : "MI"},
	  { id : 3, name : "f5", image: "13.jpeg", price : 16000, Company : "Oppo"},
	  { id : 4, name : "5T", image: "14.jpeg", price : 38000, Company : "oneplus"},
	  { id : 5, name : "iphone 6S", image: "15.jpeg", price : 80000, Company : "apple"},
	  { id : 6, name : "Z-pad", image: "16.jpeg", price : 48000, Company : "Blackberry"},
	  { id : 7, name : "M5", image: "17.jpeg", price : 6000, Company : "Celkon"},
	],
	price : ['0:10000','10000:50000','50000:10000000'],
		pricename : ['less than 10000','10000-500000','Above 50000'] },
	{ name : 'Televisions' , products : [
	  { id : 1, name : "bravia", image: "21.jpeg", price : 55000, Company : "Onida"},
	  { id : 2, name : "dolby 4k", image: "22.jpeg", price : 24000, Company : "Samsung"},
	  { id : 3, name : "home theatre", image: "23.jpeg", price : 12000, Company : "LG"},
	  { id : 4, name : "led", image: "24.jpeg", price : 150000, Company : "Micromax"},
	  { id : 5, name : "lcd", image: "25.jpeg", price : 40000, Company : "LG"},
	  { id : 6, name : "oled", image: "26.jpeg", price : 60000, Company : "Panasonic"},
	  { id : 7, name : "smart", image: "27.jpeg", price : 80000, Company : "Onida"},
	],
	price : ['0:25000','25000:75000','75000:10000000'],
		pricename : ['less than 25000','25000-750000','Above 75000'] },
	{ name : 'Mobile-Cases' , products : [
	  { id : 1, name : "redmi note", image: "31.jpeg", price : 500, Company : "ibase"},
	  { id : 2, name : "iphone", image: "32.jpeg", price : 450, Company : "rugger"},
	  { id : 3, name : "fancy", image: "33.jpeg", price : 400, Company : "armor"},
	  { id : 4, name : "hard cover", image: "34.jpeg", price : 150, Company : "VIP"},
	  { id : 5, name : "trendy", image: "35.jpeg", price : 100, Company : "maker"},
	  { id : 6, name : "tyre case", image: "36.jpeg", price : 800, Company : "leather"},
	  { id : 7, name : "shatterproof case", image: "37.jpeg", price : 1000, Company : "breathe"},
	],
	price : ['0:250','250:750','750:10000000'],
		pricename : ['less than 250','250-750','Above 750'] },

	{ name : 'Pendrives' , products : [
	  { id : 1, name : "8GB", image: "41.jpeg", price : 400, Company : "Sandisk"},
	  { id : 1, name : "8GB", image: "42.jpeg", price : 300, Company : "HP"},
	  { id : 1, name : "32GB", image: "43.jpeg", price : 990, Company : "Logo"},
	  { id : 1, name : "32GB", image: "44.jpeg", price : 1000, Company : "Moserbaer"},
	  { id : 1, name : "64GB", image: "45.jpeg", price : 1600, Company : "Ironman"},
	  { id : 1, name : "64GB", image: "47.jpeg", price : 1950, Company : "Transcend"},
	],
		price : ['0:750','750:1500','1500:10000000'],
		pricename : ['less than 750','750-1500','Above 1500'] } 
];