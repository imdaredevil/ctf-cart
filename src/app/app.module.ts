import { BrowserModule } from '@angular/platform-browser';
import { NgModule,PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { ProductsPanelComponent } from './products-panel/products-panel.component';
import { SidePanelComponent } from './side-panel/side-panel.component';
import { CategoryDisplayComponent } from './category-display/category-display.component';
import { RightSideComponent } from './right-side/right-side.component';
import { AppRoutingModule } from './/app-routing.module';
import { ProductDisplayComponent } from './product-display/product-display.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { SearchDisplayComponent } from './search-display/search-display.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductsPanelComponent,
    SidePanelComponent,
    CategoryDisplayComponent,
    RightSideComponent,
    ProductDisplayComponent,
    SearchFormComponent,
    SearchDisplayComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ctf-cart' }),
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
    constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
