import { Component, OnInit } from '@angular/core';
import { Product } from '../Product';
import { sampleproducts } from '../sampleproducts';

@Component({
  selector: 'app-right-side',
  templateUrl: './right-side.component.html',
  styleUrls: ['./right-side.component.css']
})
export class RightSideComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
	title : string = "CTF-CART";
   cate = sampleproducts;  
}
