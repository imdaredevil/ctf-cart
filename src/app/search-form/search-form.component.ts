import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { Location } from '@angular/common';
import { Product } from '../Product';
import { Category } from '../Category';
import { sampleproducts } from '../sampleproducts';


@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  cate : number;
  cat  : Category;
  
  constructor( 

    private route: ActivatedRoute,
      private location: Location,
      private router: Router ) {

       }

  ngOnInit() {
        var id = (window.location.href).split('/');
       if(id.length >= 4 && id['3'] == 'category')
            {this.cate = id['4'];
              this.cat = sampleproducts[this.cate]
            }
      console.log(this.cate);

  }

  searchcriteria(form: NgForm){
  		var url = JSON.stringify(form.value);
  		url = "/category/" + this.cate + "/search/" + url;
  		this.router.navigateByUrl(url);
      window.location.reload(true);
  }

}
