import { Component, OnInit,Input } from '@angular/core';
import { Product } from '../Product';
import { sampleproducts } from '../sampleproducts';

@Component({
  selector: 'app-products-panel',
  templateUrl: './products-panel.component.html',
  styleUrls: ['./products-panel.component.css']
})
export class ProductsPanelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  	
   title : string = "CTF-CART";
   cate = sampleproducts;  
}
